import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ProjectAmazonUnipe {
    private WebDriver driver;

    @Before
    public void abrir() {
        System.setProperty("webdriver.gecko.driver", "C:/driver/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");

    }
@After
    public void sair() {
        driver.quit();
}
@Test
    public void maisVendidos()throws InterruptedException {
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
    Thread.sleep(3000);
    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
Thread.sleep(3000);
    Assert.assertEquals("Mais vendidos", driver.findElement(By.id("zg_banner_text")).getText());
Thread.sleep(3000);
}

@Test
    public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
    }

    @Test
    public void testarAutenticacaoSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("shuashuahushashuashuahusha654@gmail.com");
        driver.findElement(By.id("continue")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Houve um problema", driver.findElement(By.className("a-alert-heading")).getText());
    }
@Test
    public void testarBuscarLivro() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/div[1]/header/div/div[4]/div[2]/div[2]/div/a[5]")).click();
        Thread.sleep(3000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("java");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(3000);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
}
@Test
    public void testarNovidadesAmazon() throws InterruptedException {
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[3]/a")).click();
    Thread.sleep(3000);
    Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
}
@Test
public void testarProdutosEmAlta() throws InterruptedException {
    driver.findElement(By.id("nav-hamburger-menu")).click();
    Thread.sleep(3000);
    driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[4]/a")).click();
    Thread.sleep(3000);
    Assert.assertEquals("Produtos em alta", driver.findElement(By.id("zg_banner_text")).getText());

}
    @Test
    public void maisDevolucoesEPedidos()throws InterruptedException {
        driver.findElement(By.id("nav-orders")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Fazer login", driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/form/div/div/div/h1")).getText());
        Thread.sleep(3000);
    }
    }